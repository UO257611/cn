# -*- coding: utf-8 -*-
"""
Editor de Spyder
"""

import numpy as np

#comment example
#%% Example 1

x=4.35
y = np.fix(x)


print (y)
#%% Example 2

a = range(2,8)
b = np.arange(2.,8.)
c = a[::-1]
d = b[::-1]

#%% Example 3

n1 = len(c)
n2 = len(d)

if n1 == 6 and n2 == 6 :
    print "hola"
elif n2 == 6 or n1 == 6 :
    print "bye"
else:
    print "none"
    
#%% Example 4

for i in range(3):
    print i

#%% Example 5

k = 0
while k<4:
    k+=1
    print k
    
#%% Example 6

l = []
k=0
while k < 4:
    k +=1
    l.append(k)
l.reverse()
print l

#%% Example 7

p = int(9.34)
q = np.fix(9.34)
r = str(9.34)

#%% Example 8

def sq(x):
    square = x*x
    return square

print sq(4.556)

#%% Example 9

print [1, 2, 3]+ [1,2,3]
print np.array([1,2,3])+np.array([1,2,3])


    





































#%% Exercise 1




























































