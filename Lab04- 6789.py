#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Hp
#
# Created:     07/03/2018
# Copyright:   (c) Hp 2018
# Licence:     <your licence>
#-----------------
from __future__ import division
import scipy.optimize as sc
import numpy as np
import matplotlib.pyplot as plt
import sympy as sym

def bisection(f,a,b, tol=1e-12,maxiter=100 ):
    counter = 0
    tempMiddle = a
    middle =a

    while(counter < maxiter):
        middle = (a+b)/2
        if(np.abs(middle -tempMiddle)< tol):
            break
        else:
            if (f(a) * f(middle) < 0 ):
                b = middle

            elif (f(middle)*f(b)  < 0):
                a = middle

            else:
                break
        tempMiddle = middle
        counter+= 1
    print("Iterations done in bisection method:" +str(counter))
    return middle


def newton(f,df,x0,tol=1e-12,maxiter=100):
    xk = x0
    counter = 0
    while( counter < maxiter):
        temp = xk
        xk = temp - (f(temp)/df(temp))
        if ( np.abs(xk -temp)< tol ):
            break

        if(xk == 0):
            break
        counter += 1

    print("Iterations done in newton method:" +str(counter))
    return xk

def secant(f,x0,x1,tol=1e-12,maxiter=100):
    counter = 0
    temp =  0
    while (counter < maxiter):



        xk = x0 - f(x0)*((x0 - x1)/(f(x0)-f(x1)))

        x1= x0
        x0 = xk
        if(xk == 0):
            break
        if(np.abs(xk -temp)< tol):
            break
        counter += 1
        temp = xk
    print("Iterations done in the secant method:" +str(counter))
    return xk





def Exercise6():

    f = lambda x: np.sin(x) -0.1*x
    df =lambda x:  np.cos(x) - 0.1
    xlist = np.arange(-10.0, 10.0,0.2 )
    ylist = []
    tlist = []

    for i in xlist:
        ylist.append(f(i))
        tlist.append(0)

    plt.plot(xlist, tlist)
    plt.plot(xlist,ylist)
    listAproximation = [-9,2.4, 5.1,7.5 ]
    for j in listAproximation:

        newtonPy = sc.newton(f, j, df,tol=1e-12)

        myNewton = newton(f, df,j )

        print ("For nearest zero of point:"+str(j)+":")
        print("Scipy solution: "+str(newtonPy))
        print("My newton: "+ str(myNewton))
        print()
        plt.plot(newtonPy, 0 , '-ro', label= "Scipy method")
        plt.plot(myNewton, 0 ,'-bo', label = "My method")

    plt.legend()
    plt.show()


def Exercise7():
    f = lambda x: np.cosh(x)*np.cos(x) - 1
    df = lambda x : np.sinh(x)*np.cos(x) + np.cosh(x)* (-np.sin(x))

    xlist = np.arange(-5.0, 5.0,0.2 )
    ylist = []
    tlist = []

    for i in xlist:
        ylist.append(f(i))
        tlist.append(0)

    plt.plot(xlist, tlist)
    plt.plot(xlist, ylist)

    myAprox = [-5, 4.5]
    for j in myAprox:

        newtonPy = sc.newton(f, j, df,tol=1e-12)
        secantPy = sc.newton(f, j ,tol=1e-12)

        myNewton = newton(f, df,j )
        mySecant = secant(f, j, j+0.5)

        print ("For nearest zero of point:"+str(j)+":")
        print("Scipy newton solution: "+str(newtonPy))
        print("Scipy secant solution: "+str(secantPy))

        print("My newton: "+ str(myNewton))
        print("My secant: "+ str(mySecant))
        print()
        plt.plot(newtonPy, 0 , '-ro', label= "Scipy newton")
        plt.plot(secantPy, 0 , '-ro', label= "Scipy secant")
        plt.plot(myNewton, 0 ,'-bo', label = "My newtown")
        plt.plot(mySecant, 0 ,'-go', label = "My secant")


    plt.show()




def Exercise8():
    f = lambda x: x**3 -75


    xlist = np.arange(-5.0, 5.0,0.2 )
    ylist = []
    tlist = []

    for i in xlist:
        ylist.append(f(i))
        tlist.append(0)

    plt.plot(xlist, tlist)
    plt.plot(xlist, ylist)

    bisectPy = sc.bisect(f, 3 ,5)
    myBisect = bisection(f, 3., 5.)
    plt.plot(bisectPy, 0 , '-ro', label= "Scipy bisection")
    plt.plot(myBisect, 0 , '-bo', label= "My bisection")
    plt.legend()
    plt.show()



def Exercise9():
    x = sym.Symbol('x', real=True)
    f_sym = x**3 + sym.log(x + 7)* sym.cos(4*x) -1
    df_sym = sym.diff(f_sym,x)
    d2f_sym = sym.diff(df_sym,x)



    f   = sym.lambdify([x], f_sym,'numpy')
    df  = sym.lambdify([x], df_sym,'numpy')
    d2f = sym.lambdify([x], d2f_sym,'numpy')



    xlist = np.arange(-2.0, 1,0.1 )
    ylist = []
    tlist = []

    for i in xlist:
        ylist.append(f(i))
        tlist.append(0)
    plt.plot(xlist, tlist)
    plt.plot(xlist, ylist)




    plt.show()


Exercise9()




