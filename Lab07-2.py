# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 19:02:42 2018

@author: UO257611
"""



from __future__ import division
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


I = Image.open('lena.jpg')
a = np.asarray(I)
plt.imshow(a)

I1 = I.convert('L')
a1 = np.asarray(I1, dtype = np.float64)

#%%

M = np.zeros(a1.shape)

i = 0

while i< a1.shape[0]:
    i+=1
    j = 0;
    while j<a1.shape[1] :
    
        if((j-250)**2)+((i-256)**2)<150**2:
            M[i][j] =1
        j+=1
        
    

b1 = a1*M

plt.imshow(b1, cmap = 'gray') 
plt.show()

i=0
while i< a1.shape[0]:
    
    j = 0;
    while j<a1.shape[1] :
    
        if((j-250)**2)+((i-256)**2)<150**2:
            M[i][j] =1
        else:
            M[i][j] =0.5
        j+=1
    i+=1
    
c1 = a1*M

plt.imshow(c1, cmap = 'gray') 
plt.show()

       

