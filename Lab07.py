# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 18:08:02 2018

@author: UO257611
"""

from __future__ import division
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


I = Image.open('lena.jpg')
a = np.asarray(I)
plt.imshow(a)

I1 = I.convert('L')
a1 = np.asarray(I1, dtype = np.float64)
plt.imshow(a1,cmap ='gray')
plt.colorbar()
plt.show()

#Normalize the matrix
M = np.max(a1)
m = np.min(a1)
a1 =(a1-m)/(M-m)

a1 = a1*255
#To uint8

a1 = a1.astype(np.uint8)
#to uimage
I11 = Image.fromarray(a1) 

#store it 
I11.save("I11.jpg")

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------



I = Image.open('lena.jpg')
a = np.asarray(I)
plt.imshow(a)

I1 = I.convert('L')
a1 = np.asarray(I1, dtype = np.float64)
plt.imshow(a1,cmap ='gray')
plt.colorbar()
plt.show()


Lena_eye=a1[251:283,317:349]
plt.imshow(Lena_eye,cmap ='gray')
plt.show()




plt.subplot(121)
plt.imshow(Lena_eye,cmap = 'gray')
plt.title("Lena eye")
plt.axis('off')


































