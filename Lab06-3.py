# encoding=utf8  
"""
Created on Tue Apr  3 12:28:42 2018

@author: els
"""
from __future__ import division
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt

np.set_printoptions(precision = 2)  
np.set_printoptions(suppress = True) 

def exercise3(func, degree ):
    matrix  = np.zeros([degree+1,degree+1])
    rightHand = np.zeros(degree+1)
    i = 0
    j = 0
    currentFunc = func
    while i<=degree:
        while j <=degree:
            currentFunc = lambda x: x**(i+j)
            result, err = sc.integrate.quad(currentFunc, -1,1)
            matrix[i][j] = result            
            
            j+=1
        i+=1
        j=0
        currentFunc = lambda x: func(x)**i
    print(matrix)
    print("------------------")
    
    i = 0
    while i<=degree:
        currentFunc = lambda x: (x**i)*func(x)
        result, err = sc.integrate.quad(currentFunc,-1,1)
        rightHand[i] = result
        i+=1
    print(rightHand)
    print("------------------")
    solution = np.linalg.solve(matrix, rightHand)
    solution = solution[::-1]    
    print(solution)
    
    x = np.linspace(-1,1)
    y = np.polyval(solution, x)
    
    plt.plot(x,y)
    plt.plot(x,func(x))
    plt.show()
 
exercise3(lambda x: np.cos(x),2)

func =lambda x: np.cos(np.arctan(x))- np.exp(x**2)*np.log(x+2)

exercise3(func, 4)




