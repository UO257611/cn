# -*- coding: utf-8 -*-


from __future__ import division


"""
Created on Wed Feb 21 18:10:40 2018

@author: UO257611
"""

"""
How to declare lambdas:
    lambda x : (function)
"""

import scipy.optimize as op
import numpy as np
import matplotlib.pyplot as plt


f = lambda x: x**3 -2*x**2 +1
df = lambda x: 3*x**2- 4*x

x0 = 2
output = op.newton(f,x0,df,tol=1.e-6)
print(output)
#%%

a = 4/3
b = 4./3.
x = np.linspace(-1,2) # define the mesh in (-1,2)
OX = 0*x             
y = f(x)

plt.plot(x,y)         # plot the function
plt.plot(x,OX,'k-', label = 'f function')   # plot the X axis


x = [1,2,3]
y = [0,3,1]

plt.plot(x,y,'ro-', label= ' points' )

plt.xlabel("X axis")
plt.ylabel("Y axis")
plt.title("Graph", fontsize = 16)
plt.plot(1,2,'o-', label = 'isolated point')

plt.legend()
plt.show()


