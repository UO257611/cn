# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 18:20:46 2018

@author: UO257611
"""

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate.quad as inte

np.set_printoptions(precision = 2)   # only 2 fractionary digits
np.set_printoptions(suppress = True) # do not use exponential notation

## matrix.T transpose
## np.dot(1Matrix, 2Matrix) multiplication
def exercise1(x0, x1, x2, x3, x4, f):
    parameters= np.array([x0,x1,x2,x3,x4])
    yvector = np.zeros(5)
    normalMatrix = np.zeros((3,5))
    index = 0
    while index < normalMatrix[0].size:
        normalMatrix[0][index] = 1
        index +=1
    
    index = 0
    for i in parameters:
        normalMatrix[1][index] = i
        index+=1
        
    index = 0
    for i in parameters:
        normalMatrix[2][index] = i**2
        index+=1

    transposeMatrix  = normalMatrix.T
    
    coeficients = np.dot(normalMatrix, transposeMatrix)
    

    yvector= parameters
    yFinal = f(yvector)
    yFinal = yFinal.T
    
    rightHandSolution = np.dot(normalMatrix, yFinal)    
    print(rightHandSolution)
    print("--------------------------------------------------")
    print(coeficients)
    
    solution = np.linalg.solve(coeficients, rightHandSolution)
    solution = solution[::-1]
    print("--------------------------------------------------")
    print(solution)
    
    x = np.linspace(-1,1)
    y = np.polyval(solution, x)
    
    plt.plot(x,y)
    
    y = np.polyval(solution, parameters)
    plt.plot(parameters,y, 'ro')
    plt.show()
    
    
    
    
def exercise2(x,d, f):
    parameters= x
    yvector = np.zeros(x.size)
    normalMatrix = np.zeros((d+1,x.size))
    index = 0
    while index < normalMatrix[0].size:
        normalMatrix[0][index] = 1
        index +=1

    x=1
    while  x < d+1:
        index = 0
        for i in parameters:
            normalMatrix[x][index] = i**x
            index+=1
        x+=1
        
  
    transposeMatrix  = normalMatrix.T
    
    coeficients = np.dot(normalMatrix, transposeMatrix)
    

    yvector= parameters
    yFinal = f(yvector)
    yFinal = yFinal.T
    
    rightHandSolution = np.dot(normalMatrix, yFinal)    
    print(rightHandSolution)
    print("--------------------------------------------------")
    print(coeficients)
    
    solution = np.linalg.solve(coeficients, rightHandSolution)
    solution = solution[::-1]
    print("--------------------------------------------------")
    print(solution)
    
    x = np.linspace(-1,1)
    y = np.polyval(solution, x)
    
    plt.plot(x,y)
    
    y = np.polyval(solution, parameters)
    plt.plot(parameters,y, 'ro')
    plt.show()
    



#    
#exercise1(-1,-0.5,0,0.5,1, lambda x: np.cos(x))
#
#points = np.linspace(-1,1,10)
#exercise2(points ,4, lambda x: np.cos(np.arctan(x)) - np.exp(np.power(x,2))* np.log( x + 2 ))



















