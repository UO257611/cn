# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 18:06:23 2018

@author: UO257611
"""


from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

#### The data #####################################
np.random.seed(7)

x1 = np.random.standard_normal((100,2))*0.6+np.ones((100,2))
x2 = np.random.standard_normal((100,2))*0.5-np.ones((100,2))
x3 = np.random.standard_normal((100,2))*0.4-2*np.ones((100,2))+5
X = np.concatenate((x1,x2,x3),axis=0)

plt.plot(X[:,0],X[:,1],'k.')
plt.show()

### ACTUAL ALGORITHM ###################

n= 3
m,d = X.shape
centroids = np.zeros((n, d))

for i in range(d):
    centroids[:,i] = np.random.rand(n)
    Ma = np.max(X)
    mi = np.min(X)
    centroids[:,i] =  centroids[:,i]*(Ma-mi)+mi
    
x1 = np.random.standard_normal((100,2))*0.6+np.ones((100,2))
x2 = np.random.standard_normal((100,2))*0.5-np.ones((100,2))
x3 = np.random.standard_normal((100,2))*0.4-2*np.ones((100,2))+5
X = np.concatenate((x1,x2,x3),axis=0)

plt.plot(X[:,0],X[:,1],'k.')
plt.plot(centroids[:,0], centroids[:,1],'mo')
plt.show()

### ASSIGN THE POINTS TO THE CENTROIDS #####

labels = np.zeros((m,))

## Using euclidean distance
k=0
while(k<100):
    total0 = 0
    total1 = 0
    total2 = 0
    pointer = 0;
    
    for point in X:
       total0 = ((centroids[0,0]-point[0])**2)+((centroids[0,1]-point[1])**2)
       total1 =  ((centroids[1,0]-point[0])**2)+((centroids[1,1]-point[1])**2)
       total2 = ((centroids[2,0]-point[0])**2)+((centroids[2,1]-point[1])**2)
       
       if total0 <= total1 and total0 <= total2:
           labels[pointer]= 0
       elif total1 <= total0 and total1 <= total2:
           labels[pointer]= 1
       elif total2 <= total0 and total2 <= total1:
           labels[pointer] = 2
       pointer+=1
       
       
    c0x= 0
    c1x= 0
    c2x= 0
    c0y= 0
    c1y= 0
    c2y= 0
    counter0=0
    counter1= 0
    counter2=0
    
    
    for i in range(X.shape[0]):
            if(labels[i] == 0):
                c0x += X[i,0] 
                c0y += X[i,1]
                counter0 += 1
            if(labels[i] == 1):
                c1x += X[i,0] 
                c1y += X[i,1]
                counter1 += 1
            if(labels[i] == 2):
                c2x += X[i,0] 
                c2y += X[i,1]
                counter2 += 1
              
    centroids[0,0] = c0x/counter0
    centroids[0,1] = c0y/counter0 
    
    centroids[1,0] = c1x/counter1
    centroids[1,1] = c1y/counter1
    
    centroids[2,0] = c2x/counter2
    centroids[2,1] = c2y/counter2
    k+=1


## Representatiton
plt.plot(X[labels==0,0],X[labels==0,1],'r.', label='cluster 1')
plt.plot(X[labels==1,0],X[labels==1,1],'b.', label='cluster 2')
plt.plot(X[labels==2,0],X[labels==2,1],'g.', label='cluster 3')

plt.plot(centroids[:,0],centroids[:,1],'mo',markersize=8, label='centroids')

plt.legend(loc='best')
plt.show()