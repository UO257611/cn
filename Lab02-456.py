# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 18:30:40 2018

@author: UO257611
"""
import sys
import numpy as np


def Exercise4a():
    counter = 1
    for i in range(1,53):
        counter = counter + ( 1*(2**(-i)))
    counter =counter*( 2**(1023))
    return counter

def Exercise4b():
    return str((2**(-1022)))


def Exercise5a(a, b, c):
    solution = -((2*c)/(np.sqrt((b**2)-4*a*c)+b))
    return solution


def Exercise5b(a,b,c):
    solution = 2*c/(-b + np.sqrt((b**2)-4*a*c))
    return solution

def Residual(a,b,c,x):
    solution1 = a*x**2+ b*x+c
    return solution1


##print(Exercise5a(1,10**8,1))
##print(Residual(1,10**8,1,Exercise5a(1,10**8,1)))

##print(Exercise5b(1,-10**8,1))
##print(Residual(1,-10**8,1,Exercise5b(1,-10**8,1)))

def Exercise6(x):
    counter= 1

    sum = False
    tempError= -1

    maxIteration = 1000
    finalLog = x
    while counter <  maxIteration:
        counter = counter + 1

        if sum:
            finalLog  += (x**counter)/counter
            sum = False
        else:
             finalLog -= (x**counter)/counter
             sum = True
        finalError = np.abs(finalLog - np.log(1.5))
        if tempError == finalError:
            break
        tempError = finalError
        print (finalError)
        print(tempError)
    print(counter)
    print(finalError)





