# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 19:33:39 2018

@author: UO257611
"""


from __future__ import division
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


I = Image.open('lena.jpg')
a = np.asarray(I)


I1 = I.convert('L')
a1 = np.asarray(I1, dtype = np.float64)



M = np.zeros(a1.shape)

n = np.linspace(0,1,a1.shape[0])
for i in range(a1.shape[0]):
    M[i] = n

M = M.transpose()

a1 = M*a1

plt.imshow(a1, cmap='gray')
