# -*- coding: utf-8 -*-
"""
Created on Wed Feb 07 18:59:59 2018

@author: UO257611d
"""


import numpy as np

def binaryDiv(x):
    solutionDivision =[]
    number = np.fix(x)
    
    while number > 0:
        binaryNumber= str( int(number % 2))
        number = np.fix(number//2)
        solutionDivision.append(binaryNumber)
    
    solutionDivision.reverse()
    return solutionDivision

#%% Exercise 2

def binaryDec(x):
    decimal = x- np.fix(x)
    solutionDecimal = []
    
    while decimal > 0:
        decimal *= 2
        if decimal >=1:
            solutionDecimal.append("1")
            decimal -=1
        else:
            solutionDecimal.append("0")
    
    return solutionDecimal



#%% Excerise 3

def exercise3(x):

    y = abs(x)
    
    binary1 = binaryDiv(y)
    binary2 = binaryDec(y)
    
    
    exponentOriginal = len(binary1) - 1
    exponent = exponentOriginal + 127
    exponent = binaryDiv(exponent)
    
    
    while len(exponent)<8:
        exponent.insert(0, "0")
    
    
    
    mantissa = binary1 + binary2
    mantissa = mantissa[1:24]
    
    
    while len(mantissa) < 23:
        mantissa.append('0')
    
    
    if(x > 0):
        sign = "0"
    else:
        sign = "1"
    
    signs = []
    signs.append(sign)
    
    finalList = []
    finalList.append("Sign")
    finalList.append(signs)
    finalList.append("Exponent")
    finalList.append(exponent)
    finalList.append("Mantissa")
    finalList.append(mantissa)
    
    for i in finalList:
        print i



exercise3(120.875)