# -*- coding: utf-8 -*-

from __future__ import division

"""
Created on Wed Feb 21 18:43:24 2018

@author: UO257611
"""

import scipy.optimize as op
import numpy as np
import matplotlib.pyplot as plt

def bisection(f,a,b, tol=1e-12,maxiter=100 ):
    counter = 0
    tempMiddle = 0
    middle =0
    while(counter < maxiter):
        middle = (a+b)/2
        if(np.abs(middle -tempMiddle)< tol):
            break
        else:
            if (f(a) * f(middle) < 0 ):
                b = middle
                
            elif (f(middle)*f(b)  < 0):
                a = middle
                
            else:
                break
        tempMiddle = middle
        counter+= 1
    print("Iterations done in bisection method:" +str(counter))
    return middle

f= lambda x: x**3 -2*x**2 +1

print( bisection(f,-2,0))
print( bisection(f,0.5,1.25))
print( bisection(f,1.25,2))

#%%
def newton(f,df,x0,tol=1e-12,maxiter=100):   
    xk = x0
    counter = 0
    while( counter < maxiter):
        temp = xk
        xk = temp - (f(temp)/df(temp))   
        if ( np.abs(xk -temp)< tol ):
            break
        
        if(xk == 0):
            break
        counter += 1
    
    print("Iterations done in newton method:" +str(counter))
    return xk

df = lambda x: 3*x**2 - 4*x



print( newton(f,df,-1 ))  
print( newton(f,df,0.5 )) 
print( newton(f,df,1.35 )) 


#%%



def secant(f,x0,x1,tol=1e-12,maxiter=100):
    counter = 0
    temp =  0
    while (counter < maxiter):
       
        
        
        xk = x0 - f(x0)*((x0 - x1)/(f(x0)-f(x1)))
        
        x1= x0
        x0 = xk
        if(xk == 0):
            break
        if(np.abs(xk -temp)< tol):
            break
        counter += 1
        temp = xk
    print("Iterations done in the secant method:" +str(counter))
    return xk

print(secant(f, -1, -0.2))
print(secant(f, 0.5,1.25))
print(secant(f, 1.35, 1.6))


 