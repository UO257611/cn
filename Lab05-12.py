# -*- coding: utf-8 -*-
"""
Created on Wed Mar 07 18:30:05 2018

@author: UO257611
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

def Vandermonde(x):
    n = x.size
    van = np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            van[i][j] = x[i]**j
    
   
    print("MY SOLUTIONS")
    print(str(van))

    
    return van
    
    
#    
#    
#x = np.array([2.,3.,4.,5.,6.])
#y = np.array([2.,6.,5.,5.,6.])    
#
#
#
#x = np.array([0.,1.,2.,3.,4.,5.,6.])
#y = np.array([3.,5.,6.,5.,4.,4.,5.])
#
#
#van = Vandermonde(x) 
#pol = np.linalg.solve(van,y)
#pol = pol[::-1]
#    
#xx = np.linspace(min(x), max(x))
#
#yy = np.polyval(pol, xx)      
#
#plt.plot(xx,yy)
#
#plt.plot(x, y,'o')
#plt.show()     



#%% Exercise 2

def lagrange_fundamental(k,z,x):
 
    fundamental1 =1
    fundamental2 =1
    for i in x:        
       if(i!= x[k]):
           fundamental1 *= (z-i)
    for i in x:
        if(i!= x[k]):
            fundamental2 *= (x[k]-i)
    return fundamental1/fundamental2

def lagrange_polinomial(z,x,y):
    solution = 0
    counter = 0
    for i in y:
        solution += i*lagrange_fundamental(counter,z,x)
        counter+=1
    return solution




x = np.array([0.,1.,2.,3.,4.,5.,6.])
y = np.array([3.,5.,6.,5.,4.,4.,5.])




    
t = np.linspace(0,6)
#z =[]
#for i in t:
#    print(str(lagrange_fundamental(2, i,x)))
#    z.append(lagrange_fundamental(2, i,x))
     

z = lagrange_fundamental(2,t,x)
##plt.plot(t,z)

q = lagrange_polinomial(t, x, y)

plt.plot(x[],y[], '-ro')
plt.plot(t,q)

plt.show()
    




