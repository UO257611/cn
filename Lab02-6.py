#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Hp
#
# Created:     21/02/2018
# Copyright:   (c) Hp 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import numpy as np
import math as Math
def Exercise6():
    ##log(1 + 5)
    temp = 0.5
    sum = True;
    current = 0.5 -  (0.5**2)/2
    counter = 3

    while ( current != temp):
        print(temp)
        temp = current;
        print (temp)
        if( sum):
            current += (0.5**counter)/counter
            sum = False
        else:
            current -= (0.5**counter)/counter
            sum = True
        print(current - Math.log(1.5) )
        counter+=1
    print("Number of iterations:    %i" %counter)
    print("Solution :    %f" %current)

Exercise6()
