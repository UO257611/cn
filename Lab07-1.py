# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 18:48:48 2018

@author: UO257611
"""

#Exercise 2


from __future__ import division
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt


I = Image.open('cameraman.tif')
a = np.asarray(I)
plt.imshow(a)

I1 = I.convert('L')
a1 = np.asarray(I1, dtype = np.float64)
plt.imshow(a1,cmap ='gray')
plt.colorbar()
plt.show()


Cameraman_eye=a1[80:150, 200:270]
plt.imshow(Cameraman_eye,cmap ='gray')
plt.show()

