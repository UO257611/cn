# -*- coding: utf-8 -*-
"""
Created on Thu Apr 12 15:27:40 2018

@author: els
"""




from __future__ import division
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import sys
reload(sys)
sys.setdefaultencoding("utf-8")


M = np.zeros((500,500))


radio=250
black= True
while radio > 5:
    i=0
    j=0
    if black:
        while i< M.shape[0]:

            while j<M.shape[0] :
            
                if((j-250)**2)+((i-256)**2)<radio**2:
                    M[i][j] =0
                j+=1
                        
            i+=1
            j = 0;
        black = False
    else:
        while i< M.shape[0]:
            i+=1
            j = 0;
            while j<M.shape[0] :
            
                if((j-250)**2)+((i-256)**2)<radio**2:
                    M[i][j] =1
                j+=1
            black = True
    radio-=10

plt.imshow(M, cmap ='gray')

final = M*255
final = final.astype(np.uint8)

saveFile = Image.fromarray(final)
saveFile.save('Circles.jpg')