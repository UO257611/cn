# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 19:38:51 2018

@author: UO257611
"""

from __future__ import division
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

black= np.zeros([250,250])
white = np.ones([250,250])

M = np.zeros([2000,2000])


blackFirst = np.concatenate((black,white), axis = 1)
whiteFirst = np.concatenate((white,black), axis = 1)  

finalrowBlack = np.tile(blackFirst,4)
finalrowWhite = np.tile(whiteFirst,4)

finalrowBlack = np.transpose(finalrowBlack)
finalrowWhite = np.transpose(finalrowWhite)

finalCombination =  np.concatenate((finalrowWhite, finalrowBlack),axis = 1)

final = np.tile(finalCombination,4)

plt.imshow(final, cmap='gray')

final = final*255
final = final.astype(np.uint8)

saveFile = Image.fromarray(final)
saveFile.save('Chess.jpg')