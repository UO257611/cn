# -*- coding: utf-8 -*-
"""
Created on Wed Mar 14 18:52:02 2018

@author: UO257611
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt



def lagrange_fundamental(k,z,x):
 
    fundamental1 =1
    fundamental2 =1
    for i in x:        
       if(i!= x[k]):
           fundamental1 *= (z-i)
    for i in x:
        if(i!= x[k]):
            fundamental2 *= (x[k]-i)
    return fundamental1/fundamental2

def lagrange_polinomial(z,x,y):
    solution = 0
    counter = 0
    for i in y:
        solution += i*lagrange_fundamental(counter,z,x)
        counter+=1
    return solution

def div_dif(x,y):
    sizeY = x.size
    matrix = np.zeros((sizeY,sizeY+1))
    matrix[:,0] =x
    matrix[:,1] = y


    
    sumation = 1    
    j = 2
    i = 0
    while j <=  sizeY:
        while i < 5-sumation:
            numerator = -matrix[i][j-1]+ matrix[i+1][j-1]
            denominator = -matrix[i][0] + matrix[i+sumation][0]
            matrix[i][j]= numerator/denominator
            i+=1

        i=0
        j+=1
        sumation+=1
            
    print(matrix) 
    return matrix

def newton_polinomial(x,y,z):
    matrix = div_dif(x,y)
    result = 0
    j=1
    acumulation = 0
    while j < matrix[0].size:
        if(acumulation ==0):
            result += matrix[0][j]
        else:
            i= 0
            temp=1
            while i< acumulation:
                temp *= (z- matrix[i][0])
                i+=1
            result += (matrix[0][j] * temp)
        j+=1
        acumulation+=1
    return result
    
        
    

    
x = np.array([2.,3.,4.,5.,6.])
y = np.array([2.,6.,5.,5.,6.])



t = np.linspace(2,6)
n = newton_polinomial(x,y,t)
print("Lagrange graph")
z = lagrange_fundamental(2,t,x)
q = lagrange_polinomial(t, x, y)
plt.plot(x, y, 'ro')
plt.plot(t,q)
plt.show()

print("Newtown graph")
plt.plot(x, y, 'ro')
plt.plot(t,n)
plt.show()


div_dif(x,y)
